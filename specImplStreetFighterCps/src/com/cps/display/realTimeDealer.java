package com.cps.display;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Paths;

import com.cps.services.engine.Commande;
import com.cps.services.engine.Engine;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;


public class realTimeDealer extends Thread {

	public boolean p1left,p2left,p1right,p2right;
	public boolean p1jump,p2jump,p1crouch,p2crouch;
	public boolean p1block,p2block,p1tech1,p2tech1;
	public boolean p1tech2,p2tech2,p1tech3,p2tech3;

	public AssetDisplay rec,rec2,rechit,rechit2;
	public AssetDisplay rectech1,rectech2;

	public AssetDisplay lifebox1,lifebox2;
	public Scene scene;
	public Engine engine;

	private int wcycle1;
	private int wcycle2;

	private int techcycle1[];
	private int techcycle2[];

	private String tech1;
	private String tech2;
	
	private boolean exit=false;

	private ImagePattern dead_jackie,blockstunned_jackie
	,hitstunned_jackie,punching1_jackie,kick1_jackie,special1_jackie,
	special2_jackie,punching2_jackie,kick2_jackie,special3_jackie,punching3_jackie,
	idle_jackie,jump1_jackie,jump2_jackie,walk1_jackie,crouching_jackie,kick3_jackie,
	blocking_jackie,walk2_jackie;

	private ImagePattern dead_elsa,blockstunned_elsa
	,hitstunned_elsa,punch1_elsa,kick1_elsa,special1_elsa,
	special2_elsa,punch2_elsa,kick2_elsa,special3_elsa,punch3_elsa,
	idle_elsa,jumping1_elsa,jumping2_elsa,walk1_elsa,crouching_elsa,kick3_elsa,
	blocking_elsa,walk2_elsa;

	private ImagePattern dead_rev_jackie,blockstunned_rev_jackie
	,hitstunned_rev_jackie,punching1_rev_jackie,kick1_rev_jackie,special1_rev_jackie,
	special2_rev_jackie,punching2_rev_jackie,kick2_rev_jackie,special3_rev_jackie,punching3_rev_jackie,
	idle_rev_jackie,jump1_rev_jackie,jump2_rev_jackie,walk1_rev_jackie,crouching_rev_jackie
	,kick3_rev_jackie,blocking_rev_jackie,walk2_rev_jackie;

	private ImagePattern dead_rev_elsa,blockstunned_rev_elsa
	,hitstunned_rev_elsa,punch1_rev_elsa,kick1_rev_elsa,special1_rev_elsa,
	special2_rev_elsa,punch2_rev_elsa,kick2_rev_elsa,special3_rev_elsa,punch3_rev_elsa,
	idle_rev_elsa,jumping1_rev_elsa,jumping2_rev_elsa,walk1_rev_elsa,crouching_rev_elsa
	,kick3_rev_elsa,blocking_rev_elsa,walk2_rev_elsa;


	public realTimeDealer(AssetDisplay rec, AssetDisplay rec2, AssetDisplay rechit,
			AssetDisplay rechit2, AssetDisplay rectech1, AssetDisplay rectech2, 
			AssetDisplay lifebox1, AssetDisplay lifebox2, Scene scene, Engine engine){
		super();
		this.p1left=false;
		this.p2left=false;
		this.p1right=false;
		this.p2right=false;
		this.p1jump=false;
		this.p2jump=false;
		this.p1crouch=false;
		this.p2crouch=false;
		this.p1block=false;
		this.p2block=false;
		this.p1tech1=false;
		this.p2tech1=false;
		this.p1tech2=false;
		this.p2tech2=false;
		this.p1tech3=false;
		this.p2tech3=false;
		this.rec=rec;
		this.rec2=rec2;
		this.scene=scene;
		this.engine=engine;
		this.wcycle1=0;
		this.wcycle2=0;
		this.rechit=rechit;
		this.rechit2=rechit2;
		this.techcycle1=new int[3];
		this.techcycle2=new int[3];
		this.rectech1=rectech1;
		this.rectech2=rectech2;
		this.lifebox1=lifebox1;
		this.lifebox2=lifebox2;


		//initialisation des sprites 
		try {
			/*jackie*/
			dead_jackie = new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/dead.png"))));
			blockstunned_jackie= new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/blockstunned.png"))));
			hitstunned_jackie = new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/hitstunned.png"))));
			punching1_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/punching1.png"))));
			kick1_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/kick1.png"))));
			special1_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/special1.png"))));
			punching2_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/punching2.png"))));
			kick2_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/kick2.png"))));
			special2_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/special2.png"))));
			special3_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/special3.png"))));
			punching3_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/punching3.png"))));
			idle_jackie =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/idle.png"))));
			jump1_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/jump1.png"))));
			jump2_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/jump2.png"))));
			walk1_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/walk1.png"))));
			walk2_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/walk2.png"))));
			kick3_jackie  =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/kick3.png"))));
			blocking_jackie =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/blocking.png"))));
			crouching_jackie =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/crouching.png"))));
			/*jackie_rev*/
			dead_rev_jackie = new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/dead_rev.png"))));
			blockstunned_rev_jackie= new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/blockstunned_rev.png"))));
			hitstunned_rev_jackie = new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/hitstunned_rev.png"))));
			punching1_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/punching1_rev.png"))));
			kick1_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/kick1_rev.png"))));
			special1_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/special1_rev.png"))));
			punching2_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/punching2_rev.png"))));
			kick2_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/kick2_rev.png"))));
			special2_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/special2_rev.png"))));
			special3_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/special3_rev.png"))));
			punching3_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/punching3_rev.png"))));
			idle_rev_jackie =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/idle_rev.png"))));
			jump1_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/jump1_rev.png"))));
			jump2_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/jump2_rev.png"))));
			walk1_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/walk1_rev.png"))));
			walk2_rev_jackie=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/walk2_rev.png"))));
			kick3_rev_jackie  =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/kick3_rev.png"))));
			blocking_rev_jackie =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/blocking_rev.png"))));
			crouching_rev_jackie =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/jackie/crouching_rev.png"))));

			/*elsa*/
			dead_elsa = new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/dead.png"))));
			blockstunned_elsa= new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/blockstunned.png"))));
			hitstunned_elsa = new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/hitstunned.png"))));
			punch1_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/punch1.png"))));
			kick1_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/kick1.png"))));
			special1_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/special1.png"))));
			punch2_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/punch2.png"))));
			kick2_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/kick2.png"))));
			special2_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/special2.png"))));
			special3_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/special3.png"))));
			punch3_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/punch3.png"))));
			idle_elsa =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/idle.png"))));
			jumping1_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/jumping1.png"))));
			jumping2_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/jumping2.png"))));
			walk1_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/walk1.png"))));
			walk2_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/walk2.png"))));
			kick3_elsa  =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/kick3.png"))));
			blocking_elsa =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/blocking.png"))));
			crouching_elsa =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/crouching.png"))));


			/*elsa_rev*/
			dead_rev_elsa = new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/dead_rev.png"))));
			blockstunned_rev_elsa= new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/blockstunned_rev.png"))));
			hitstunned_rev_elsa = new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/hitstunned_rev.png"))));
			punch1_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/punch1_rev.png"))));
			kick1_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/kick1_rev.png"))));
			special1_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/special1_rev.png"))));
			punch2_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/punch2_rev.png"))));
			kick2_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/kick2_rev.png"))));
			special2_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/special2_rev.png"))));
			special3_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/special3_rev.png"))));
			punch3_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/punch3_rev.png"))));
			idle_rev_elsa =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/idle_rev.png"))));
			jumping1_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/jumping1_rev.png"))));
			jumping2_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/jumping2_rev.png"))));
			walk1_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/walk1_rev.png"))));
			walk2_rev_elsa=new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/walk2_rev.png"))));
			kick3_rev_elsa  =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/kick3_rev.png"))));
			blocking_rev_elsa =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/blocking_rev.png"))));
			crouching_rev_elsa =new ImagePattern(new Image(Files.newInputStream(
					Paths.get("res/sprites/elsa/crouching_rev.png"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.start();
	}

	public void run(){
		while(!exit){
			Commande c1=Commande.NEUTRAL;
			Commande c2=Commande.NEUTRAL;


			if (p1left){
				p1block=false;
				wcycle1++;
				c1=Commande.LEFT;
			}

			if (p2left){
				p2block=false;
				wcycle2++;
				c2=Commande.LEFT;
			}

			if (p1right){
				p1block=false;
				wcycle1++;
				c1=Commande.RIGHT;
			}

			if (p2right){
				p2block=false;
				wcycle2++;
				c2=Commande.RIGHT;
			}

			if (p1jump){
				p1block=false;
				c1=Commande.JUMP;
			}

			if (p2jump){
				p2block=false;
				c2=Commande.JUMP;
			}

			if (p1crouch){
				p1block=false;
				c1=Commande.CROUCH;
			}

			if (p2crouch){
				p2block=false;
				c2=Commande.CROUCH;
			}

			if (p1block){
				c1=Commande.BLOCK;
			}

			if (p2block){
				c2=Commande.BLOCK;
			}

			if (p1tech1){
				c1=Commande.PUNCH;
				this.tech1="punch";
			}

			if (p2tech1){
				c2=Commande.PUNCH;
				this.tech2="punch";
			}

			if (p1tech2){
				c1=Commande.KICK;
				this.tech1="kick";
			}

			if (p2tech2){
				c2=Commande.KICK;
				this.tech2="kick";
			}

			if (p1tech3){
				c1=Commande.SPECIAL;
				this.tech1="special";
			}

			if (p2tech3){
				c2=Commande.SPECIAL;
				this.tech2="special";
			}

			engine.step(c1, c2);

			if (p1tech1){
				techcycle1[0]=engine.getChar(1).tech().sframe();
				techcycle1[1]=engine.getChar(1).tech().hframe();
				techcycle1[2]=engine.getChar(1).tech().rframe();
				p1tech1=false;
			}

			if (p2tech1){
				techcycle2[0]=engine.getChar(2).tech().sframe();
				techcycle2[1]=engine.getChar(2).tech().hframe();
				techcycle2[2]=engine.getChar(2).tech().rframe();
				p2tech1=false;
			}

			if (p1tech2){
				techcycle1[0]=engine.getChar(1).tech().sframe();
				techcycle1[1]=engine.getChar(1).tech().hframe();
				techcycle1[2]=engine.getChar(1).tech().rframe();
				p1tech2=false;
			}

			if (p2tech2){
				techcycle2[0]=engine.getChar(2).tech().sframe();
				techcycle2[1]=engine.getChar(2).tech().hframe();
				techcycle2[2]=engine.getChar(2).tech().rframe();
				p2tech2=false;
			}

			if (p1tech3){
				techcycle1[0]=engine.getChar(1).tech().sframe();
				techcycle1[1]=engine.getChar(1).tech().hframe();
				techcycle1[2]=engine.getChar(1).tech().rframe();
				p1tech3=false;
			}

			if (p2tech3){
				techcycle2[0]=engine.getChar(2).tech().sframe();
				techcycle2[1]=engine.getChar(2).tech().hframe();
				techcycle2[2]=engine.getChar(2).tech().rframe();
				p2tech3=false;
			}

			try{

				if (engine.getChar(1).faceRight()){
					if (engine.getChar(1).dead()){
						rec.setFill(dead_jackie);
						this.engine.getChar(1).charBox().SetLength(123);
						this.engine.getChar(1).charBox().SetHeight(35);
						rec.setX(engine.getChar(1).positionX()-105);
						rec.setY(engine.getChar(1).positionY()+79);
						rec.setHeight(engine.getChar(1).charBox().Height());
						rec.setWidth(engine.getChar(1).charBox().Length());
						engine.getChar(1).charBox().MoveTo(engine.getChar(1).positionX()-105, engine.getChar(1).positionY()+79);
						rectech1.setStrokeWidth(0);
						
						exit=true;
					}
					else{
						if (this.engine.getChar(1).isBlockstunned()){
							rec.setFill(blockstunned_jackie);
							this.engine.getChar(1).charBox().SetLength(64);
							this.engine.getChar(1).charBox().SetHeight(103);
							rec.setX(engine.getChar(1).positionX());

							rec.setHeight(engine.getChar(1).charBox().Height());
							rec.setWidth(engine.getChar(1).charBox().Length());
							rectech1.setStrokeWidth(0);
						}
						else if (this.engine.getChar(1).isHitstunned()){
							rec.setFill(hitstunned_jackie);
							this.engine.getChar(1).charBox().SetLength(77);
							this.engine.getChar(1).charBox().SetHeight(100);
							rec.setX(engine.getChar(1).positionX());

							rec.setHeight(engine.getChar(1).charBox().Height());
							rec.setWidth(engine.getChar(1).charBox().Length());
							rectech1.setStrokeWidth(0);
						}
						else {
							if (this.engine.getChar(1).isTeching()){
								if (techcycle1[0]>0){
									if (tech1=="punch"){
										rec.setFill(punching1_jackie);
										this.engine.getChar(1).charBox().SetLength(68);
										this.engine.getChar(1).charBox().SetHeight(100);
										rec.setX(engine.getChar(1).positionX());
									}
									else if (tech1=="kick"){
										rec.setFill(kick1_jackie);
										this.engine.getChar(1).charBox().SetLength(46);
										this.engine.getChar(1).charBox().SetHeight(107);
										rec.setX(engine.getChar(1).positionX());
									}
									else if (tech1=="special"){
										rec.setFill(special1_jackie);
										this.engine.getChar(1).charBox().SetLength(50);
										this.engine.getChar(1).charBox().SetHeight(101);
										rec.setX(engine.getChar(1).positionX());
									}
									rec.setHeight(engine.getChar(1).charBox().Height());
									rec.setWidth(engine.getChar(1).charBox().Length());
									techcycle1[0]--;
									rectech1.setStrokeWidth(0);
								}
								else {
									if (techcycle1[1]>0){
										if (tech1=="punch"){
											rec.setFill(punching2_jackie);
											this.engine.getChar(1).charBox().SetLength(68);
											this.engine.getChar(1).charBox().SetHeight(97);
											rec.setX(engine.getChar(1).positionX());

											rec.setHeight(92);
											rec.setWidth(97);
										}

										else if (tech1=="kick"){
											rec.setFill(kick2_jackie);
											this.engine.getChar(1).charBox().SetLength(47);
											this.engine.getChar(1).charBox().SetHeight(111);
											rec.setX(engine.getChar(1).positionX());

											rec.setHeight(91);
											rec.setWidth(111);
										}
										else if (tech1=="special"){
											rec.setFill(special2_jackie);
											this.engine.getChar(1).charBox().SetLength(51);
											this.engine.getChar(1).charBox().SetHeight(106);
											rec.setX(engine.getChar(1).positionX());

											rec.setHeight(106);
											rec.setWidth(71);
										}
										techcycle1[1]--;
										rectech1.setX(engine.getChar(1).tech().getHitBox().PositionX());
										rectech1.setY(engine.getChar(1).tech().getHitBox().PositionY());
										rectech1.setHeight(engine.getChar(1).tech().getHitBox().Height());
										rectech1.setWidth(engine.getChar(1).tech().getHitBox().Length());
										rectech1.setStroke(Color.GREEN);
										rectech1.setStrokeWidth(2);
									}
									else {
										if (techcycle1[2]>0){
											if (tech1=="punch"){
												rec.setFill(punching3_jackie);
												this.engine.getChar(1).charBox().SetLength(65);
												this.engine.getChar(1).charBox().SetHeight(107);
												rec.setX(engine.getChar(1).positionX());
											}
											else if (tech1=="kick"){
												rec.setFill(kick3_jackie);
												this.engine.getChar(1).charBox().SetLength(77);
												this.engine.getChar(1).charBox().SetHeight(103);
												rec.setX(engine.getChar(1).positionX());
											}
											else if (tech1=="special"){
												rec.setFill(special3_jackie);
												this.engine.getChar(1).charBox().SetLength(65);
												this.engine.getChar(1).charBox().SetHeight(125);
												rec.setX(engine.getChar(1).positionX());
											}
											rec.setHeight(engine.getChar(1).charBox().Height());
											rec.setWidth(engine.getChar(1).charBox().Length());
											techcycle1[2]--;
											rectech1.setStrokeWidth(0);
										}
									}
								}
							}
							else {
								if (this.engine.getChar(1).isBlocking()){
									rec.setFill(blocking_jackie);
									this.engine.getChar(1).charBox().SetLength(57);
									this.engine.getChar(1).charBox().SetHeight(107);
									rec.setX(engine.getChar(1).positionX());

									rec.setHeight(engine.getChar(1).charBox().Height());
									rec.setWidth(engine.getChar(1).charBox().Length());
									rectech1.setStrokeWidth(0);
								}

								else {

									if (this.engine.getChar(1).getVSpeed()>0){
										rec.setFill(jump1_jackie);
										this.engine.getChar(1).charBox().SetLength(41);
										this.engine.getChar(1).charBox().SetHeight(140);
										rec.setX(engine.getChar(1).positionX());

										rec.setHeight(engine.getChar(1).charBox().Height());
										rec.setWidth(engine.getChar(1).charBox().Length());
										rectech1.setStrokeWidth(0);
									}
									else if (this.engine.getChar(1).getVSpeed()<0){
										rec.setFill(jump2_jackie);
										this.engine.getChar(1).charBox().SetLength(42);
										this.engine.getChar(1).charBox().SetHeight(115);	
										rec.setX(engine.getChar(1).positionX());

										rec.setHeight(engine.getChar(1).charBox().Height());
										rec.setWidth(engine.getChar(1).charBox().Length());
										rectech1.setStrokeWidth(0);
									}

									else{
										if (c1==Commande.NEUTRAL){
											wcycle1=0;
											rec.setFill(idle_jackie);
											this.engine.getChar(1).charBox().SetLength(30);
											this.engine.getChar(1).charBox().SetHeight(114);
											rec.setX(engine.getChar(1).positionX());

											rec.setHeight(engine.getChar(1).charBox().Height());
											rec.setWidth(engine.getChar(1).charBox().Length());
											rectech1.setStrokeWidth(0);

										}
										if ((c1==Commande.LEFT)||(c1==Commande.RIGHT)){
											if ((wcycle1%12)<6){
												rec.setFill(walk1_jackie);
												this.engine.getChar(1).charBox().SetLength(48);
												this.engine.getChar(1).charBox().SetHeight(114);
												rec.setX(engine.getChar(1).positionX());

												rec.setHeight(engine.getChar(1).charBox().Height());
												rec.setWidth(engine.getChar(1).charBox().Length());
												rectech1.setStrokeWidth(0);
											}	
											else{
												rec.setFill(walk2_jackie);
												this.engine.getChar(1).charBox().SetLength(48);
												this.engine.getChar(1).charBox().SetHeight(114);
												rec.setX(engine.getChar(1).positionX());

												rec.setHeight(engine.getChar(1).charBox().Height());
												rec.setWidth(engine.getChar(1).charBox().Length());
												rectech1.setStrokeWidth(0);
											}
										}

										if (c1==Commande.CROUCH){
											rec.setFill(crouching_jackie);
											this.engine.getChar(1).charBox().SetLength(61);
											this.engine.getChar(1).charBox().SetHeight(78);
											rec.setX(engine.getChar(1).positionX());

											rec.setHeight(engine.getChar(1).charBox().Height());
											rec.setWidth(engine.getChar(1).charBox().Length());
											rectech1.setStrokeWidth(0);
										}
									}
								}
							}
						}
						rec.setY(engine.getChar(1).positionY());
					}
				}
				else {
					if (engine.getChar(1).dead()){
						rec.setFill(dead_rev_jackie);
						this.engine.getChar(1).charBox().SetLength(123);
						this.engine.getChar(1).charBox().SetHeight(35);
						rec.setX(engine.getChar(1).positionX());
						rec.setY(engine.getChar(1).positionY()+79);
						rec.setHeight(engine.getChar(1).charBox().Height());
						rec.setWidth(engine.getChar(1).charBox().Length());
						rectech1.setStrokeWidth(0);
						engine.getChar(1).charBox().MoveTo(engine.getChar(1).positionX(), engine.getChar(1).positionY()+79);
						
						exit=true;
					}
					else{
						if (this.engine.getChar(1).isBlockstunned()){
							rec.setFill(blockstunned_rev_jackie);
							this.engine.getChar(1).charBox().SetLength(64);
							this.engine.getChar(1).charBox().SetHeight(103);
							rec.setX(engine.getChar(1).positionX());

							rec.setHeight(engine.getChar(1).charBox().Height());
							rec.setWidth(engine.getChar(1).charBox().Length());
							rectech1.setStrokeWidth(0);
						}
						else if (this.engine.getChar(1).isHitstunned()){
							rec.setFill(hitstunned_rev_jackie);
							this.engine.getChar(1).charBox().SetLength(77);
							this.engine.getChar(1).charBox().SetHeight(100);
							rec.setX(engine.getChar(1).positionX());

							rec.setHeight(engine.getChar(1).charBox().Height());
							rec.setWidth(engine.getChar(1).charBox().Length());
							rectech1.setStrokeWidth(0);
						}
						else {
							if (this.engine.getChar(1).isTeching()){
								if (techcycle1[0]>0){
									if (tech1=="punch"){
										rec.setFill(punching1_rev_jackie);
										this.engine.getChar(1).charBox().SetLength(68);
										this.engine.getChar(1).charBox().SetHeight(100);
										rec.setX(engine.getChar(1).positionX());
									}
									else if (tech1=="kick"){
										rec.setFill(kick1_rev_jackie);
										this.engine.getChar(1).charBox().SetLength(46);
										this.engine.getChar(1).charBox().SetHeight(107);
										rec.setX(engine.getChar(1).positionX());
									}
									else if (tech1=="special"){
										rec.setFill(special1_rev_jackie);
										this.engine.getChar(1).charBox().SetLength(50);
										this.engine.getChar(1).charBox().SetHeight(101);
										rec.setX(engine.getChar(1).positionX());
									}
									rec.setHeight(engine.getChar(1).charBox().Height());
									rec.setWidth(engine.getChar(1).charBox().Length());
									techcycle1[0]--;
									rectech1.setStrokeWidth(0);
								}
								else {
									if (techcycle1[1]>0){
										if (tech1=="punch"){
											rec.setFill(punching2_rev_jackie);
											this.engine.getChar(1).charBox().SetLength(68);
											this.engine.getChar(1).charBox().SetHeight(97);
											rec.setX(engine.getChar(1).positionX()-39);

											rec.setHeight(92);
											rec.setWidth(97);
										}

										else if (tech1=="kick"){
											rec.setFill(kick2_rev_jackie);
											this.engine.getChar(1).charBox().SetLength(47);
											this.engine.getChar(1).charBox().SetHeight(111);
											rec.setX(engine.getChar(1).positionX()-44);

											rec.setHeight(91);
											rec.setWidth(111);
										}
										else if (tech1=="special"){
											rec.setFill(special2_rev_jackie);
											this.engine.getChar(1).charBox().SetLength(51);
											this.engine.getChar(1).charBox().SetHeight(105);
											rec.setX(engine.getChar(1).positionX()-19);

											rec.setHeight(106);
											rec.setWidth(71);
										}
										techcycle1[1]--;
										rectech1.setX(engine.getChar(1).tech().getHitBox().PositionX());
										rectech1.setY(engine.getChar(1).tech().getHitBox().PositionY());
										rectech1.setHeight(engine.getChar(1).tech().getHitBox().Height());
										rectech1.setWidth(engine.getChar(1).tech().getHitBox().Length());
										rectech1.setStroke(Color.GREEN);
										rectech1.setStrokeWidth(2);
									}
									else {
										if (techcycle1[2]>0){
											if (tech1=="punch"){
												rec.setFill(punching3_rev_jackie);
												this.engine.getChar(1).charBox().SetLength(65);
												this.engine.getChar(1).charBox().SetHeight(107);
												rec.setX(engine.getChar(1).positionX());
											}
											else if (tech1=="kick"){
												rec.setFill(kick3_rev_jackie);
												this.engine.getChar(1).charBox().SetLength(77);
												this.engine.getChar(1).charBox().SetHeight(103);
												rec.setX(engine.getChar(1).positionX());
											}
											else if (tech1=="special"){
												rec.setFill(special3_rev_jackie);
												this.engine.getChar(1).charBox().SetLength(65);
												this.engine.getChar(1).charBox().SetHeight(125);
												rec.setX(engine.getChar(1).positionX());
											}
											rec.setHeight(engine.getChar(1).charBox().Height());
											rec.setWidth(engine.getChar(1).charBox().Length());
											techcycle1[2]--;
											rectech1.setStrokeWidth(0);
										}
									}
								}

							}
							else {
								if (this.engine.getChar(1).isBlocking()){
									rec.setFill(blocking_rev_jackie);
									this.engine.getChar(1).charBox().SetLength(57);
									this.engine.getChar(1).charBox().SetHeight(107);
									rec.setX(engine.getChar(1).positionX());

									rec.setHeight(engine.getChar(1).charBox().Height());
									rec.setWidth(engine.getChar(1).charBox().Length());
									rectech1.setStrokeWidth(0);
								}
								else {
									if (this.engine.getChar(1).getVSpeed()>0){
										rec.setFill(jump1_rev_jackie);
										this.engine.getChar(1).charBox().SetLength(41);
										this.engine.getChar(1).charBox().SetHeight(140);
										rec.setX(engine.getChar(1).positionX());

										rec.setHeight(engine.getChar(1).charBox().Height());
										rec.setWidth(engine.getChar(1).charBox().Length());
										rectech1.setStrokeWidth(0);
									}
									else if (this.engine.getChar(1).getVSpeed()<0){
										rec.setFill(jump2_rev_jackie);
										this.engine.getChar(1).charBox().SetLength(42);
										this.engine.getChar(1).charBox().SetHeight(115);
										rec.setX(engine.getChar(1).positionX());

										rec.setHeight(engine.getChar(1).charBox().Height());
										rec.setWidth(engine.getChar(1).charBox().Length());
										rectech1.setStrokeWidth(0);
									}

									else{
										if (c1==Commande.NEUTRAL){
											wcycle1=0;
											rec.setFill(idle_rev_jackie);
											this.engine.getChar(1).charBox().SetLength(30);
											this.engine.getChar(1).charBox().SetHeight(114);
											rec.setX(engine.getChar(1).positionX());

											rec.setHeight(engine.getChar(1).charBox().Height());
											rec.setWidth(engine.getChar(1).charBox().Length());
											rectech1.setStrokeWidth(0);

										}
										if ((c1==Commande.LEFT)||(c1==Commande.RIGHT)){
											if ((wcycle1%12)<6){
												rec.setFill(walk1_rev_jackie);
												this.engine.getChar(1).charBox().SetLength(48);
												this.engine.getChar(1).charBox().SetHeight(114);
												rec.setX(engine.getChar(1).positionX());

												rec.setHeight(engine.getChar(1).charBox().Height());
												rec.setWidth(engine.getChar(1).charBox().Length());
												rectech1.setStrokeWidth(0);
											}	
											else{
												rec.setFill(walk2_rev_jackie);
												this.engine.getChar(1).charBox().SetLength(48);
												this.engine.getChar(1).charBox().SetHeight(114);
												rec.setX(engine.getChar(1).positionX());

												rec.setHeight(engine.getChar(1).charBox().Height());
												rec.setWidth(engine.getChar(1).charBox().Length());
												rectech1.setStrokeWidth(0);
											}
										}
										if (c1==Commande.CROUCH){
											rec.setFill(crouching_rev_jackie);
											this.engine.getChar(1).charBox().SetLength(61);
											this.engine.getChar(1).charBox().SetHeight(78);
											rec.setX(engine.getChar(1).positionX());

											rec.setHeight(engine.getChar(1).charBox().Height());
											rec.setWidth(engine.getChar(1).charBox().Length());
											rectech1.setStrokeWidth(0);
										}
									}
								}
							}
						}
						rec.setY(engine.getChar(1).positionY());
					}
				}

				if (!engine.getChar(2).faceRight()){
					if (engine.getChar(2).dead()){
						rec2.setFill(dead_rev_elsa);
						this.engine.getChar(2).charBox().SetLength(104);
						this.engine.getChar(2).charBox().SetHeight(24);
						rec2.setX(engine.getChar(2).positionX());
						rec2.setY(engine.getChar(2).positionY()+75);
						rec2.setHeight(engine.getChar(2).charBox().Height());
						rec2.setWidth(engine.getChar(2).charBox().Length());
						rectech2.setStrokeWidth(0);
						engine.getChar(2).charBox().MoveTo(engine.getChar(2).positionX(), engine.getChar(2).positionY()+75);
						exit=true;
					}
					else{
						if (this.engine.getChar(2).isBlockstunned()){
							rec2.setFill(blockstunned_rev_elsa);
							this.engine.getChar(2).charBox().SetLength(53);
							this.engine.getChar(2).charBox().SetHeight(99);
							rec2.setX(engine.getChar(2).positionX());

							rec2.setHeight(engine.getChar(2).charBox().Height());
							rec2.setWidth(engine.getChar(2).charBox().Length());
							rectech2.setStrokeWidth(0);
						}
						else if (this.engine.getChar(2).isHitstunned()){
							rec2.setFill(hitstunned_rev_elsa);
							this.engine.getChar(2).charBox().SetLength(52);
							this.engine.getChar(2).charBox().SetHeight(75);
							rec2.setX(engine.getChar(2).positionX());

							rec2.setHeight(engine.getChar(2).charBox().Height());
							rec2.setWidth(engine.getChar(2).charBox().Length());
							rectech2.setStrokeWidth(0);
						}
						else {
							if (this.engine.getChar(2).isTeching()){
								if (techcycle2[0]>0){
									if (tech2=="punch"){
										rec2.setFill(punch1_rev_elsa);
										this.engine.getChar(2).charBox().SetLength(38);
										this.engine.getChar(2).charBox().SetHeight(64);
										rec2.setX(engine.getChar(2).positionX());
									}
									else if (tech2=="kick"){
										rec2.setFill(kick1_rev_elsa);
										this.engine.getChar(2).charBox().SetLength(67);
										this.engine.getChar(2).charBox().SetHeight(97);
										rec2.setX(engine.getChar(2).positionX());
									}
									else if (tech2=="special"){
										rec2.setFill(special1_rev_elsa);
										this.engine.getChar(2).charBox().SetLength(57);
										this.engine.getChar(2).charBox().SetHeight(90);
										rec2.setX(engine.getChar(2).positionX());
									}
									rec2.setHeight(engine.getChar(2).charBox().Height());
									rec2.setWidth(engine.getChar(2).charBox().Length());
									techcycle2[0]--;
									rectech2.setStrokeWidth(0);
								}
								else {
									if (techcycle2[1]>0){
										if (tech2=="punch"){
											rec2.setFill(punch2_rev_elsa);
											this.engine.getChar(2).charBox().SetLength(33);
											this.engine.getChar(2).charBox().SetHeight(61);
											rec2.setX(engine.getChar(2).positionX()-29);

											rec2.setHeight(61);
											rec2.setWidth(67);
										}

										else if (tech2=="kick"){
											rec2.setFill(kick2_rev_elsa);
											this.engine.getChar(2).charBox().SetLength(46);
											this.engine.getChar(2).charBox().SetHeight(90);
											rec2.setX(engine.getChar(2).positionX()-45);

											rec2.setHeight(90);
											rec2.setWidth(92);
										}
										else if (tech2=="special"){
											rec2.setFill(special2_rev_elsa);
											this.engine.getChar(2).charBox().SetLength(43);
											this.engine.getChar(2).charBox().SetHeight(97);
											rec2.setX(engine.getChar(2).positionX()-55);

											rec2.setHeight(97);
											rec2.setWidth(74);
										}
										techcycle2[1]--;
										rectech2.setX(engine.getChar(2).tech().getHitBox().PositionX());
										rectech2.setY(engine.getChar(2).tech().getHitBox().PositionY());
										rectech2.setHeight(engine.getChar(2).tech().getHitBox().Height());
										rectech2.setWidth(engine.getChar(2).tech().getHitBox().Length());
										rectech2.setStroke(Color.YELLOW);
										rectech2.setStrokeWidth(2);
									}
									else {
										if (techcycle2[2]>0){
											if (tech2=="punch"){
												rec2.setFill(punch3_rev_elsa);
												this.engine.getChar(2).charBox().SetLength(38);
												this.engine.getChar(2).charBox().SetHeight(64);
												rec2.setX(engine.getChar(2).positionX());
											}
											else if (tech2=="kick"){
												rec2.setFill(kick3_rev_elsa);
												this.engine.getChar(2).charBox().SetLength(34);
												this.engine.getChar(2).charBox().SetHeight(98);
												rec2.setX(engine.getChar(2).positionX());
											}
											else if (tech2=="special"){
												rec2.setFill(special3_rev_elsa);
												this.engine.getChar(2).charBox().SetLength(70);
												this.engine.getChar(2).charBox().SetHeight(91);
												rec2.setX(engine.getChar(2).positionX());
											}
											rec2.setHeight(engine.getChar(2).charBox().Height());
											rec2.setWidth(engine.getChar(2).charBox().Length());
											techcycle2[2]--;
											rectech2.setStrokeWidth(0);
										}
									}
								}
							}

							else {
								if (this.engine.getChar(2).isBlocking()){
									rec2.setFill(blocking_rev_elsa);
									this.engine.getChar(2).charBox().SetLength(64);
									this.engine.getChar(2).charBox().SetHeight(99);
									rec2.setX(engine.getChar(2).positionX());

									rec2.setHeight(engine.getChar(2).charBox().Height());
									rec2.setWidth(engine.getChar(2).charBox().Length());
									rectech2.setStrokeWidth(0);
								}
								else {
									if (this.engine.getChar(2).getVSpeed()>0){
										rec2.setFill(jumping1_rev_elsa);
										this.engine.getChar(2).charBox().SetLength(47);
										this.engine.getChar(2).charBox().SetHeight(76);
										rec2.setX(engine.getChar(2).positionX());

										rec2.setHeight(engine.getChar(2).charBox().Height());
										rec2.setWidth(engine.getChar(2).charBox().Length());
										rectech2.setStrokeWidth(0);
									}
									else if (this.engine.getChar(2).getVSpeed()<0){
										rec2.setFill(jumping2_rev_elsa);
										this.engine.getChar(2).charBox().SetLength(76);
										this.engine.getChar(2).charBox().SetHeight(115);
										rec2.setX(engine.getChar(2).positionX());

										rec2.setHeight(engine.getChar(2).charBox().Height());
										rec2.setWidth(engine.getChar(2).charBox().Length());
										rectech2.setStrokeWidth(0);
									}

									else{
										if (c2==Commande.NEUTRAL){
											wcycle2=0;
											rec2.setFill(idle_rev_elsa);
											this.engine.getChar(2).charBox().SetLength(46);
											this.engine.getChar(2).charBox().SetHeight(99);
											rec2.setX(engine.getChar(2).positionX());

											rec2.setHeight(engine.getChar(2).charBox().Height());
											rec2.setWidth(engine.getChar(2).charBox().Length());
											rectech2.setStrokeWidth(0);
										}
										if ((c2==Commande.LEFT)||(c2==Commande.RIGHT)){
											if ((wcycle2%12)<6){
												rec2.setFill(walk1_rev_elsa);
												this.engine.getChar(2).charBox().SetLength(59);
												this.engine.getChar(2).charBox().SetHeight(99);
												rec2.setX(engine.getChar(2).positionX());

												rec2.setHeight(engine.getChar(2).charBox().Height());
												rec2.setWidth(engine.getChar(2).charBox().Length());
												rectech2.setStrokeWidth(0);
											}	
											else{
												rec2.setFill(walk2_rev_elsa);
												this.engine.getChar(2).charBox().SetLength(58);
												this.engine.getChar(2).charBox().SetHeight(99);
												rec2.setX(engine.getChar(2).positionX());

												rec2.setHeight(engine.getChar(2).charBox().Height());
												rec2.setWidth(engine.getChar(2).charBox().Length());
												rectech2.setStrokeWidth(0);
											}
										}

										if (c2==Commande.CROUCH){
											rec2.setFill(crouching_rev_elsa);
											this.engine.getChar(2).charBox().SetLength(38);
											this.engine.getChar(2).charBox().SetHeight(64);
											rec2.setX(engine.getChar(2).positionX());

											rec2.setHeight(engine.getChar(2).charBox().Height());
											rec2.setWidth(engine.getChar(2).charBox().Length());
											rectech2.setStrokeWidth(0);
										}
									}
								}
							}
						}
						rec2.setY(engine.getChar(2).positionY());
					}
				}


				else {
					if (engine.getChar(2).dead()){
						rec2.setFill(dead_elsa);
						this.engine.getChar(2).charBox().SetLength(104);
						this.engine.getChar(2).charBox().SetHeight(24);
						rec2.setX(engine.getChar(2).positionX()-58);
						rec2.setY(engine.getChar(2).positionY()+75);
						rec2.setHeight(engine.getChar(2).charBox().Height());
						rec2.setWidth(engine.getChar(2).charBox().Length());
						rectech2.setStrokeWidth(0);
						engine.getChar(2).charBox().MoveTo(engine.getChar(2).positionX()-58, engine.getChar(2).positionY()+75);
						
						exit=true;
					}
					else{
						if (this.engine.getChar(2).isBlockstunned()){
							rec2.setFill(blockstunned_elsa);
							this.engine.getChar(2).charBox().SetLength(53);
							this.engine.getChar(2).charBox().SetHeight(99);
							rec2.setX(engine.getChar(2).positionX());

							rec2.setHeight(engine.getChar(2).charBox().Height());
							rec2.setWidth(engine.getChar(2).charBox().Length());
							rectech2.setStrokeWidth(0);
						}
						else if (this.engine.getChar(2).isHitstunned()){
							rec2.setFill(hitstunned_elsa);
							this.engine.getChar(2).charBox().SetLength(52);
							this.engine.getChar(2).charBox().SetHeight(75);
							rec2.setX(engine.getChar(2).positionX());

							rec2.setHeight(engine.getChar(2).charBox().Height());
							rec2.setWidth(engine.getChar(2).charBox().Length());
							rectech2.setStrokeWidth(0);
						}
						else {
							if (this.engine.getChar(2).isTeching()){
								if (techcycle2[0]>0){
									if (tech2=="punch"){
										rec2.setFill(punch1_elsa);
										this.engine.getChar(2).charBox().SetLength(38);
										this.engine.getChar(2).charBox().SetHeight(64);
										rec2.setX(engine.getChar(2).positionX());
									}
									else if (tech2=="kick"){
										rec2.setFill(kick1_elsa);
										this.engine.getChar(2).charBox().SetLength(67);
										this.engine.getChar(2).charBox().SetHeight(97);
										rec2.setX(engine.getChar(2).positionX());
									}
									else if (tech2=="special"){
										rec2.setFill(special1_elsa);
										this.engine.getChar(2).charBox().SetLength(57);
										this.engine.getChar(2).charBox().SetHeight(90);
										rec2.setX(engine.getChar(2).positionX());
									}
									rec2.setHeight(engine.getChar(2).charBox().Height());
									rec2.setWidth(engine.getChar(2).charBox().Length());
									techcycle2[0]--;
									rectech2.setStrokeWidth(0);
								}
								else {
									if (techcycle2[1]>0){
										if (tech2=="punch"){
											rec2.setFill(punch2_elsa);
											this.engine.getChar(2).charBox().SetLength(33);
											this.engine.getChar(2).charBox().SetHeight(61);
											rec2.setX(engine.getChar(2).positionX());

											rec2.setHeight(61);
											rec2.setWidth(67);
										}

										else if (tech2=="kick"){
											rec2.setFill(kick2_elsa);
											this.engine.getChar(2).charBox().SetLength(46);
											this.engine.getChar(2).charBox().SetHeight(90);
											rec2.setX(engine.getChar(2).positionX());

											rec2.setHeight(90);
											rec2.setWidth(92);
										}
										else if (tech2=="special"){
											rec2.setFill(special2_elsa);
											this.engine.getChar(2).charBox().SetLength(43);
											this.engine.getChar(2).charBox().SetHeight(97);
											rec2.setX(engine.getChar(2).positionX());

											rec2.setHeight(97);
											rec2.setWidth(74);
										}
										techcycle2[1]--;
										rectech2.setX(engine.getChar(2).tech().getHitBox().PositionX());
										rectech2.setY(engine.getChar(2).tech().getHitBox().PositionY());
										rectech2.setHeight(engine.getChar(2).tech().getHitBox().Height());
										rectech2.setWidth(engine.getChar(2).tech().getHitBox().Length());
										rectech2.setStroke(Color.YELLOW);
										rectech2.setStrokeWidth(2);
									}
									else {
										if (techcycle2[2]>0){
											if (tech2=="punch"){
												rec2.setFill(punch3_elsa);
												this.engine.getChar(2).charBox().SetLength(38);
												this.engine.getChar(2).charBox().SetHeight(64);
												rec2.setX(engine.getChar(2).positionX());
											}
											else if (tech2=="kick"){
												rec2.setFill(kick3_elsa);
												this.engine.getChar(2).charBox().SetLength(34);
												this.engine.getChar(2).charBox().SetHeight(98);
												rec2.setX(engine.getChar(2).positionX());
											}
											else if (tech2=="special"){
												rec2.setFill(special3_elsa);
												this.engine.getChar(2).charBox().SetLength(70);
												this.engine.getChar(2).charBox().SetHeight(91);
												rec2.setX(engine.getChar(2).positionX());
											}
											rec2.setHeight(engine.getChar(2).charBox().Height());
											rec2.setWidth(engine.getChar(2).charBox().Length());
											techcycle2[2]--;
											rectech2.setStrokeWidth(0);
										}
									}
								}
							}

							else {
								if (this.engine.getChar(2).isBlocking()){
									rec2.setFill(blocking_elsa);
									this.engine.getChar(2).charBox().SetLength(64);
									this.engine.getChar(2).charBox().SetHeight(99);
									rec2.setX(engine.getChar(2).positionX());

									rec2.setHeight(engine.getChar(2).charBox().Height());
									rec2.setWidth(engine.getChar(2).charBox().Length());
									rectech2.setStrokeWidth(0);
								}
								else {
									if (this.engine.getChar(2).getVSpeed()>0){
										rec2.setFill(jumping1_elsa);
										this.engine.getChar(2).charBox().SetLength(47);
										this.engine.getChar(2).charBox().SetHeight(76);
										rec2.setX(engine.getChar(2).positionX());

										rec2.setHeight(engine.getChar(2).charBox().Height());
										rec2.setWidth(engine.getChar(2).charBox().Length());
										rectech2.setStrokeWidth(0);
									}
									else if (this.engine.getChar(2).getVSpeed()<0){
										rec2.setFill(jumping2_elsa);
										this.engine.getChar(2).charBox().SetLength(76);
										this.engine.getChar(2).charBox().SetHeight(115);
										rec2.setX(engine.getChar(2).positionX());

										rec2.setHeight(engine.getChar(2).charBox().Height());
										rec2.setWidth(engine.getChar(2).charBox().Length());
										rectech2.setStrokeWidth(0);
									}

									else{
										if (c2==Commande.NEUTRAL){
											wcycle2=0;
											rec2.setFill(idle_elsa);
											this.engine.getChar(2).charBox().SetLength(46);
											this.engine.getChar(2).charBox().SetHeight(99);
											rec2.setX(engine.getChar(2).positionX());

											rec2.setHeight(engine.getChar(2).charBox().Height());
											rec2.setWidth(engine.getChar(2).charBox().Length());
											rectech2.setStrokeWidth(0);
										}
										if ((c2==Commande.LEFT)||(c2==Commande.RIGHT)){
											if ((wcycle2%12)<6){
												rec2.setFill(walk1_elsa);
												this.engine.getChar(2).charBox().SetLength(59);
												this.engine.getChar(2).charBox().SetHeight(99);
												rec2.setX(engine.getChar(2).positionX());

												rec2.setHeight(engine.getChar(2).charBox().Height());
												rec2.setWidth(engine.getChar(2).charBox().Length());
												rectech2.setStrokeWidth(0);
											}	
											else{
												rec2.setFill(walk2_elsa);
												this.engine.getChar(2).charBox().SetLength(58);
												this.engine.getChar(2).charBox().SetHeight(99);
												rec2.setX(engine.getChar(2).positionX());

												rec2.setHeight(engine.getChar(2).charBox().Height());
												rec2.setWidth(engine.getChar(2).charBox().Length());
												rectech2.setStrokeWidth(0);
											}
										}

										if (c2==Commande.CROUCH){
											rec2.setFill(crouching_elsa);
											this.engine.getChar(2).charBox().SetLength(38);
											this.engine.getChar(2).charBox().SetHeight(64);
											rec2.setX(engine.getChar(2).positionX());

											rec2.setHeight(engine.getChar(2).charBox().Height());
											rec2.setWidth(engine.getChar(2).charBox().Length());
											rectech2.setStrokeWidth(0);
										}
									}
								}
							}
						}
						rec2.setY(engine.getChar(2).positionY());
					}
				}
			} catch (Exception e){
				System.out.println("no care");
			}




			rechit.setX(engine.getChar(1).charBox().PositionX());
			rechit.setY(engine.getChar(1).charBox().PositionY());
			rechit.setHeight(engine.getChar(1).charBox().Height());
			rechit.setWidth(engine.getChar(1).charBox().Length());



			rechit2.setX(engine.getChar(2).charBox().PositionX());
			rechit2.setY(engine.getChar(2).charBox().PositionY());
			rechit2.setHeight(engine.getChar(2).charBox().Height());
			rechit2.setWidth(engine.getChar(2).charBox().Length());
			rechit.setX(engine.getChar(1).charBox().PositionX());
			rechit.setY(engine.getChar(1).charBox().PositionY());
			rechit.setHeight(engine.getChar(1).charBox().Height());
			rechit.setWidth(engine.getChar(1).charBox().Length());


			lifebox1.setWidth(engine.getChar(1).life());

			lifebox2.setX(engine.getWidth()-5-engine.getChar(2).life());
			lifebox2.setWidth(engine.getChar(2).life());


			try{
				Thread.sleep(50);
			} catch (InterruptedException e){
				System.err.println("Issue with sleep");
			}
		}
	}
}
